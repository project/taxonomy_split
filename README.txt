$Name$

This is the README for taxonomy_split module
This module is for Drupal 6.x, other versions may follow.

It is useful if you have a vocabulary in the form of a tree,
eg it has terms within terms with nodes attached to them.

It will place all the first level terms in a block and
when a term is clicked on displays the tree below that term
in another block.

If panels_mini 3.x is installed taxonomy_split will install
a minipanel with it's own layout. This has three columns,
the first shows the first level, the second shows the children
of the selected parent and the third column shows the teasers
of any nodes found below the term selected.
The outputs are all themed.

This module makes use of json callbacks to speed things up.
