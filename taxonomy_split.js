// $Name$

/**
 * @file
 * Javascript functions for taxonomy_split
 *
 * @author Bob Hutchinson http://drupal.org/user/52366
 */

Drupal.behaviors.taxonomy_split = function() {
  allowed_vids = Drupal.settings['taxonomy_split']['allowed_vids'];
  for(c=0; c < allowed_vids.length; c++) {
    $("#block-taxonomy_split-taxonomy_split_child_" + allowed_vids[c]).hide();
  }
}

// from parent block to child block
function taxonomy_split_block_parent_click(num, tname, vid) {
  var u = "http://"+Drupal.settings['taxonomy_split']['host']+Drupal.settings['basePath']+'taxonomy_split_get_data';
  $.get(u,
    {tid:num, typ:'block'},
    function(data) {
      if (data['error'] != undefined && data['error'] != '') {
        alert(data['error']);
      }
      else {
        var content = data['content'];
        $("#block-taxonomy_split-taxonomy_split_child_"+vid+" div.content").html(content);
        $("#block-taxonomy_split-taxonomy_split_child_"+vid+" h2.title").html(tname);
        $("#block-taxonomy_split-taxonomy_split_child_"+vid+"").show();
        $("span.tsb_trig").removeClass('active');
        $("#tsb-" + num + "-" + vid).addClass('active');
      }
    },
    'json'
  );
}

// from panel 1 to panel 2
function taxonomy_split_panel_parent_click(num, tname, vid) {
  var u = "http://"+Drupal.settings['taxonomy_split']['host']+Drupal.settings['basePath']+'taxonomy_split_get_data';
  $.get(u,
    {tid:num, typ:'panel'},
    function(data) {
      if (data['error'] != undefined && data['error'] != '') {
        alert(data['error']);
      }
      else {
        var content = data['content'];
        $("#ts_middle_"+vid+" div.content").html(content);
        $("#ts_middle_"+vid+" h2").html(tname);
        $("span.tsp_trig").removeClass('active');
        $("#tsp-" + num + "-" + vid).addClass('active');
      }
    },
    'json'
  );
}

// from panel 2 to panel 3
function taxonomy_split_panel2_parent_click(num, tname, vid, p) {
  var u = "http://"+Drupal.settings['taxonomy_split']['host']+Drupal.settings['basePath']+'taxonomy_split_get_data';
  $.get(u,
    {tid:num, typ:'panel2', page:p},
    function(data) {
      if (data['error'] != undefined && data['error'] != '') {
        alert(data['error']);
      }
      else {
        var content = data['content'];
        $("#ts_right_"+vid+" div.content").html(content);
        $("#ts_right_"+vid+" h2").html(tname);
        $("span.tsc_trig").removeClass('active');
        $("#tspc-" + num + "-" + vid).addClass('active');
      }
    },
    'json'
  );
}

// give the child block click something to do
function taxonomy_split_block2_parent_click(cssid) {
//  $('#' + cssid + ' .expanded').

}
