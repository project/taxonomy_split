<?php
// $Name$

/**
 * @file
 * Theming
 * @author Bob Hutchinson http://drupal.org/user/52366
 */

/**
 * Theming
 *
 *
 */
function taxonomy_split_theme() {
  return array(
    'taxonomy_split_children' => array(
      'arguments' => array(
        'tree' => NULL,
        'type' => NULL,
      ),
    ),
    'taxonomy_split_parents' => array(
      'arguments' => array(
        'tree' => NULL,
        'type' => NULL,
      ),
    ),
    'taxonomy_split_nodes' => array(
      'arguments' => array(
        'tid' => NULL,
        'page' => NULL,
      ),
    ),
    'taxonomy_split_node' => array(
      'arguments' => array(
        'nid' => NULL,
      ),
    ),
    'taxonomy_split_children2' => array(
      'arguments' => array(
        'tree' => NULL,
      ),
    ),
    'taxonomy_split_panel2' => array(
      'arguments' => array(
        'tree' => NULL,
        'page' => NULL,
      ),
    ),
    'taxonomy_split_node_link' => array(
      'arguments' => array(
        'link' => NULL,
      ),
    ),
    // from common.inc for pager.inc
    'taxonomy_split_pager' => array(
      'arguments' => array(
        'tags' => array(),
        'limit' => 10,
        'element' => 0,
        'parameters' => array(),
      ),
    ),
    'taxonomy_split_pager_first' => array(
      'arguments' => array(
        'text' => NULL,
        'limit' => NULL,
        'element' => 0,
        'parameters' => array(),
      ),
    ),
    'taxonomy_split_pager_previous' => array(
      'arguments' => array(
        'text' => NULL,
        'limit' => NULL,
        'element' => 0,
        'interval' => 1,
        'parameters' => array(),
      ),
    ),
    'taxonomy_split_pager_next' => array(
      'arguments' => array(
        'text' => NULL,
        'limit' => NULL,
        'element' => 0,
        'interval' => 1,
        'parameters' => array(),
      ),
    ),
    'taxonomy_split_pager_last' => array(
      'arguments' => array(
        'text' => NULL,
        'limit' => NULL,
        'element' => 0,
        'parameters' => array(),
      ),
    ),
    'taxonomy_split_pager_link' => array(
      'arguments' => array(
        'text' => NULL,
        'page_new' => NULL,
        'element' => NULL,
        'parameters' => array(),
        'attributes' => array(),
      ),
    ),
  );
}

/**
 * Theme a child block or panel
 *
 * @param obj $tree
 *
 * @return themed string
 *   .
 *
 */
function theme_taxonomy_split_children($tree, $type = 'block') {
  $parent = 0;
  $depth = 0;
  $old_depth = 0;
  $output .= "<ul class=\"menu\">\n";

  foreach ($tree AS $i => $obj) {
    $parent = $obj->parents[0];
    $depth = $obj->depth;
    $tid = $obj->tid;
    $vid = taxonomy_split_get_vid_by_tid($tid);
    $name = $obj->name;
    $count = 0;
    $extramsg = '';
    $extraclick = '';
    $cssid = '';
    $licssid = '';
    if ( isset($obj->count) ) {
      $count = $obj->count;
      if ($type == 'block') {
        $licssid = "tsbc-$tid-$vid";
        $extraclick = ' onClick="taxonomy_split_block2_parent_click(\''. $licssid . '\')"';
      }
      elseif ($type == 'panel') {
        $cssid = "tspc-$tid-$vid";
        $extraclick = ' id="'. $cssid .'" onClick="taxonomy_split_panel2_parent_click('. $tid . ', \''. $name .'\', '. $vid .')"';
      }
      $extramsg = ' has ' . format_plural($count, '1 entry', '@count entries');
    }
    // check for children
    if ($depth > $old_depth) {
      $output .= "<ul class=\"menu\">\n";
    }
    elseif ($depth < $old_depth) {
      $delta = $old_depth - $depth;
      $output .= str_repeat("</ul>\n", $delta);
    }
    //
    $old_depth = $depth;
    // look for nodes
    if (isset($obj->link)) {
      $link =  $obj->link;
    }
    else {
      $link = '<span class="tsc_trig" '. $extraclick . ' title="'. $name . $extramsg . '">' . $name . '</span>';
    }
    if ($depth < $tree[$i+1]->depth) {
      $class = 'expanded';
    }
    else {
      $class = 'leaf';
    }

    if (variable_get('taxonomy_split_settings_show', 1)) {
      $output .= '<li class="'. $class .'">'. $link ."</li>\n";
    }
    elseif (isset($obj->count) || isset($obj->link ) ) {
      $output .= '<li '. ($licssid ? 'id="'. $licssid .'"' : '') .' class="'. $class .'">'. $link ."</li>\n";
    }
  }
  if ($depth >= 1) {
    $output .= '</ul></li>'."\n";
  }
  if ($depth == 0) {
    $output .= '</ul>'."\n";
  }
  $output .= str_repeat("</ul>\n", $depth);
  return $output;
}

/**
 * Theme a parent block or panel
 *
 * @param obj $tree
 * @param string $type
 *
 * @return themed string
 *   .
 *
 */
function theme_taxonomy_split_parents($tree, $type = 'block') {
  $output = "<ul class=\"menu\">\n";
  foreach ($tree AS $i => $obj) {
    $printit = FALSE;
    $vid = taxonomy_split_get_vid_by_tid($obj->tid);
    // skip node entries
    if (! isset($obj->parents[0])) {
      continue;
    }
    // only want top level terms
    if ($obj->parents[0] == 0) {
      // skip empties ?
      if (variable_get('taxonomy_split_settings_show', 1)) {
        // show all
        if ($tree[$i]->depth < $tree[$i+1]->depth) {
          $class = 'leaf';
          $printit = TRUE;
        }
      }
      else {
        // skip empties
        if ($tree[$i]->depth < $tree[$i+1]->depth && isset($tree[$i]->count) ) {
          $class = 'leaf';
          $printit = TRUE;
        }
      }
      if ($printit) {
        if ($type == 'block') {
          $cssid = "tsb-". $obj->tid ."-". $vid;
          $trig = "tsb_trig";
        }
        else {
          $cssid = "tsp-". $obj->tid ."-". $vid;
          $trig = "tsp_trig";
        }
        $output .= '<li class="'. $class .'">';
        $output .= '<span id="'. $cssid .'" onClick="taxonomy_split_'. $type .'_parent_click('. $obj->tid . ', \''. $obj->name .'\', '. $vid .')" class="'. $trig .'">' . $obj->name . '</span>';
        $output .= '</li>'."\n";
      }
    }
  }
  $output .= "</ul>\n";
  return $output;
}

/**
 * Theme node teasers
 *
 * @param obj $tree
 *
 * @return themed string
 *   .
 *
 */
function theme_taxonomy_split_nodes($tid, $page) {
  $nodes = taxonomy_split_fetch_nodes($tid, FALSE, $page);

  $output = '';
  foreach ($nodes AS  $node) {
    $nid = $node['nid'];
    $output .= theme('taxonomy_split_node', $nid);
  }
  $maxpage = variable_get('taxonomy_split_panel_maxpage', TAXONOMY_SPLIT_MAXPAGE);
  $pager = theme('taxonomy_split_pager', NULL, $maxpage, 0, array(), 0);
  $output .= $pager;
  return $output;
}

/**
 * Theme list of node links
 *
 * @param obj $tree
 *
 * @return themed string
 *   .
 *
 */
function theme_taxonomy_split_children2($tree) {
  $output = '';
  $nodes = array();
  foreach ($tree AS $i => $obj) {
    // look for nodes, weed out dupes
    if (isset($obj->link)) {
      if (! isset($nodes[$obj->nid])) {
        $nodes[$obj->nid]['link'] = $obj->link;
      }
    }
  }
  if (count($nodes)) {
    foreach ($nodes AS $nid => $node) {
      $output .= theme('taxonomy_split_node_link', $node['link']);
    }
  }

  return $output;
}

function theme_taxonomy_split_panel2($tree, $page) {
  $output = '';
  $nodes = array();
  foreach ($tree AS $i => $obj) {
    // look for nodes, weed out dupes
    if (isset($obj->link)) {
      if (! isset($nodes[$obj->nid])) {
        $nodes[$obj->nid]['link'] = $obj->link;
      }
    }
  }
  foreach ($nodes AS $nid => $node) {
      $output .= theme('taxonomy_split_node_link', $node['link']);
  }
  return $output;
}

/**
 * Theme node teaser
 *
 * @param int $nid
 *
 * @return themed string
 *   .
 *
 */
function theme_taxonomy_split_node($nid) {
  $output = '';
  if ($nid) {
    $node = node_load($nid);
    $teaser = node_teaser($node->body);
    $title = $node->title;
    $uid = $node->uid;
    $output .= '<fieldset class="ts_nodefs">';
    $output .= '<h3>' . $title . '</h3>';
    $output .= $teaser;
    $output .= '<p>';
    $output .= l(t('Details'), 'node/'. $nid);
    $output .= "&nbsp;|&nbsp;";
    $output .= l(t('Contact'), 'user/'. $uid .'/contact');
    $output .= '</p>';
    $output .= '</fieldset>';
  }
  return $output;
}

/**
 * Theme a link
 *
 * @param int $nid
 *
 * @return themed string
 *   .
 *
 */
function theme_taxonomy_split_node_link($link) {
  $output = '';
  if ($link) {
    $output .= $link .'<br />';
  }
  return $output;
}

// custom pager theming to work with json
function theme_taxonomy_split_pager_link($text, $page_new, $element, $parameters = array(), $attributes = array()) {
  $page = (isset($_GET['page']) && is_numeric($_GET['page']) ? $_GET['page'] : 0);
  $tid = (isset($_GET['tid']) && is_numeric($_GET['tid']) ? $_GET['tid'] : '');
  $vid = taxonomy_split_get_vid_by_tid($tid);
  $name = taxonomy_split_get_vid_name($vid);
  if ($new_page = implode(',', pager_load_array($page_new[$element], $element, explode(',', $page)))) {
    $parameters['page'] = $new_page;
  }

  $query = array();
  if (count($parameters)) {
    $query[] = drupal_query_string_encode($parameters, array());
  }
  $querystring = pager_get_querystring();
  if ($querystring != '') {
    $query[] = $querystring;
  }

  // Set each pager link title
  if (!isset($attributes['title'])) {
    static $titles = NULL;
    if (!isset($titles)) {
      $titles = array(
        t('« first') => t('Go to first page'),
        t('‹ previous') => t('Go to previous page'),
        t('next ›') => t('Go to next page'),
        t('last »') => t('Go to last page'),
      );
    }
    if (isset($titles[$text])) {
      $attributes['title'] = $titles[$text];
    }
    elseif (is_numeric($text)) {
      $attributes['title'] = t('Go to page @number', array('@number' => $text));
    }
  }
  $link = '<span onClick="taxonomy_split_panel2_parent_click('. $tid . ', \''. $name .'\', '. $vid .', '. $new_page .')" class="tsc_trig" title="'. $titles[$text] .'" >'. $text .'</span>';
  return $link;
}

function theme_taxonomy_split_pager($tags = array(), $limit = 10, $element = 0, $parameters = array(), $quantity = 9) {
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_first = theme('taxonomy_split_pager_first', (isset($tags[0]) ? $tags[0] : t('« first')), $limit, $element, $parameters);
  $li_previous = theme('taxonomy_split_pager_previous', (isset($tags[1]) ? $tags[1] : t('‹ previous')), $limit, $element, 1, $parameters);
  $li_next = theme('taxonomy_split_pager_next', (isset($tags[3]) ? $tags[3] : t('next ›')), $limit, $element, 1, $parameters);
  $li_last = theme('taxonomy_split_pager_last', (isset($tags[4]) ? $tags[4] : t('last »')), $limit, $element, $parameters);

  if ($pager_total[$element] > 1) {
    if ($li_first) {
      $items[] = array(
        'class' => 'pager-first',
        'data' => $li_first,
      );
    }
    if ($li_previous) {
      $items[] = array(
        'class' => 'pager-previous',
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => 'pager-ellipsis',
          'data' => '…',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => 'pager-item',
            'data' => theme('taxonomy_split_pager_previous', $i, $limit, $element, ($pager_current - $i), $parameters),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => 'pager-current',
            'data' => $i,
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => 'pager-item',
            'data' => theme('taxonomy_split_pager_next', $i, $limit, $element, ($i - $pager_current), $parameters),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => 'pager-ellipsis',
          'data' => '…',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => 'pager-next',
        'data' => $li_next,
      );
    }
    if ($li_last) {
      $items[] = array(
        'class' => 'pager-last',
        'data' => $li_last,
      );
    }
    return theme('item_list', $items, NULL, 'ul', array('class' => 'pager'));
  }
}

function theme_taxonomy_split_pager_first($text, $limit, $element = 0, $parameters = array()) {
  global $pager_page_array;
  $output = '';

  // If we are anywhere but the first page
  if ($pager_page_array[$element] > 0) {
    $output = theme('taxonomy_split_pager_link', $text, pager_load_array(0, $element, $pager_page_array), $element, $parameters);
  }

  return $output;
}

function theme_taxonomy_split_pager_last($text, $limit, $element = 0, $parameters = array()) {
  global $pager_page_array, $pager_total;
  $output = '';

  // If we are anywhere but the last page
  if ($pager_page_array[$element] < ($pager_total[$element] - 1)) {
    $output = theme('taxonomy_split_pager_link', $text, pager_load_array($pager_total[$element] - 1, $element, $pager_page_array), $element, $parameters);
  }

  return $output;
}

function theme_taxonomy_split_pager_next($text, $limit, $element = 0, $interval = 1, $parameters = array()) {
  global $pager_page_array, $pager_total;
  $output = '';

  // If we are anywhere but the last page
  if ($pager_page_array[$element] < ($pager_total[$element] - 1)) {
    $page_new = pager_load_array($pager_page_array[$element] + $interval, $element, $pager_page_array);
    // If the next page is the last page, mark the link as such.
    if ($page_new[$element] == ($pager_total[$element] - 1)) {
      $output = theme('taxonomy_split_pager_last', $text, $limit, $element, $parameters);
    }
    // The next page is not the last page.
    else {
      $output = theme('taxonomy_split_pager_link', $text, $page_new, $element, $parameters);
    }
  }

  return $output;
}

function theme_taxonomy_split_pager_previous($text, $limit, $element = 0, $interval = 1, $parameters = array()) {
  global $pager_page_array;
  $output = '';

  // If we are anywhere but the first page
  if ($pager_page_array[$element] > 0) {
    $page_new = pager_load_array($pager_page_array[$element] - $interval, $element, $pager_page_array);

    // If the previous page is the first page, mark the link as such.
    if ($page_new[$element] == 0) {
      $output = theme('taxonomy_split_pager_first', $text, $limit, $element, $parameters);
    }
    // The previous page is not the first page.
    else {
      $output = theme('taxonomy_split_pager_link', $text, $page_new, $element, $parameters);
    }
  }

  return $output;
}

// helper functions, might not need this
/*
function taxonomy_split_pager_load_array($value, $element, $old_array) {
  $new_array = $old_array;
  // Look for empty elements.
  for ($i = 0; $i < $element; $i++) {
    if (!$new_array[$i]) {
      // Load found empty element with 0.
      $new_array[$i] = 0;
    }
  }
  // Update the changed element.
  $new_array[$element] = (int)$value;
  return $new_array;
}

*/
