<?php
// $Name$

/**
 * @file
 * Define a panels layout.
 * @author Bob Hutchinson http://drupal.org/user/52366
 */

/**
 * Implementation of hook_panels_layouts().
 */
// Plugin definition
$plugin = array(
  'title' => t('Taxonomy_split '),
  'icon' => 'taxonomy_split.png',
  'theme' => 'panels-taxonomy_split',
  'theme arguments' => array('id', 'content'),
  'css' => 'taxonomy_split.css',
  'panels' => array(
    'left' => t('Left side'),
    'middle' => t('Middle column'),
    'right' => t('Right side')
  ),
);
